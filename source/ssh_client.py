# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ssh_client.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_SSH_Client(object):
    def setupUi(self, SSH_Client):
        SSH_Client.setObjectName("SSH_Client")
        SSH_Client.resize(400, 233)
        self.formLayoutWidget = QtWidgets.QWidget(SSH_Client)
        self.formLayoutWidget.setGeometry(QtCore.QRect(10, 21, 381, 207))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.SSH_Data = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.SSH_Data.setContentsMargins(0, 0, 0, 0)
        self.SSH_Data.setObjectName("SSH_Data")
        self.User_Text = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.User_Text.setObjectName("User_Text")
        self.SSH_Data.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.User_Text)
        self.User_Name = QtWidgets.QLabel(self.formLayoutWidget)
        self.User_Name.setObjectName("User_Name")
        self.SSH_Data.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.User_Name)
        self.IP_Address = QtWidgets.QLabel(self.formLayoutWidget)
        self.IP_Address.setObjectName("IP_Address")
        self.SSH_Data.setWidget(6, QtWidgets.QFormLayout.LabelRole, self.IP_Address)
        self.IP_Text = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.IP_Text.setObjectName("IP_Text")
        self.SSH_Data.setWidget(6, QtWidgets.QFormLayout.FieldRole, self.IP_Text)
        self.Passphrase = QtWidgets.QLabel(self.formLayoutWidget)
        self.Passphrase.setObjectName("Passphrase")
        self.SSH_Data.setWidget(7, QtWidgets.QFormLayout.LabelRole, self.Passphrase)
        self.Passphrase_Text = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.Passphrase_Text.setObjectName("Passphrase_Text")
        self.SSH_Data.setWidget(7, QtWidgets.QFormLayout.FieldRole, self.Passphrase_Text)
        self.Connect = QtWidgets.QPushButton(self.formLayoutWidget)
        self.Connect.setObjectName("Connect")
        self.SSH_Data.setWidget(8, QtWidgets.QFormLayout.LabelRole, self.Connect)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.SSH_Data.setItem(3, QtWidgets.QFormLayout.SpanningRole, spacerItem)
        self.SSH_Client_Title_Display = QtWidgets.QLabel(self.formLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(24)
        font.setBold(True)
        font.setWeight(75)
        self.SSH_Client_Title_Display.setFont(font)
        self.SSH_Client_Title_Display.setAlignment(QtCore.Qt.AlignCenter)
        self.SSH_Client_Title_Display.setObjectName("SSH_Client_Title_Display")
        self.SSH_Data.setWidget(2, QtWidgets.QFormLayout.SpanningRole, self.SSH_Client_Title_Display)

        self.retranslateUi(SSH_Client)
        QtCore.QMetaObject.connectSlotsByName(SSH_Client)

    def retranslateUi(self, SSH_Client):
        _translate = QtCore.QCoreApplication.translate
        SSH_Client.setWindowTitle(_translate("SSH_Client", "Dialog"))
        self.User_Name.setText(_translate("SSH_Client", "User:"))
        self.IP_Address.setText(_translate("SSH_Client", "IP:"))
        self.Passphrase.setText(_translate("SSH_Client", "Password:"))
        self.Connect.setText(_translate("SSH_Client", "Connect"))
        self.SSH_Client_Title_Display.setWhatsThis(_translate("SSH_Client", "<html><head/><body><p align=\"center\"><br/></p></body></html>"))
        self.SSH_Client_Title_Display.setText(_translate("SSH_Client", "SSH Client"))

